﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    public MusicGroups musicGroupChanged;
    private MusicGroups oldMusicGroup;
    public float bpm = 120;
    private float bps = 0;

    public float normal = 0;
    public float confident = 0;
    public float danger = 0;
    public int computerDelayMS = 0;

    private float oldnormal = 0;
    private float oldconfident = 0;
    private float olddanger = 0;

    private float[] valuesPercussion = { 0, 0, 0 };
    private float[] valuesWoodwinds = { 0, 0, 0 };
    private float[] valuesStrings = { 0, 0, 0 };
    private float[] valuesBrass = { 0, 0, 0 };


    public List<MusicSample> musicSamples;

    [FMODUnity.EventRef]
    public string startingEventPercussion;
    [FMODUnity.EventRef]
    public string startingEventWoodwinds;
    [FMODUnity.EventRef]
    public string startingEventStrings;
    [FMODUnity.EventRef]
    public string startingEventBrass;

    private FMOD.Studio.EventInstance eventPercussion;
    private FMOD.Studio.EventInstance eventWoodwinds;
    private FMOD.Studio.EventInstance eventStrings;
    private FMOD.Studio.EventInstance eventBrass;

    private bool audioClipPercussionChanged = false;
    private bool audioClipWoodwindsChanged = false;
    private bool audioClipStringsChanged = false;
    private bool audioClipBrassChanged = false;

    private FMOD.Studio.EventInstance newEventPercussion;
    private FMOD.Studio.EventInstance newEventWoodwinds;
    private FMOD.Studio.EventInstance newEventStrings;
    private FMOD.Studio.EventInstance newEventBrass;

    [System.Serializable]
    public class MusicSample
    {
        [FMODUnity.EventRef]
        public string eventToBePlayed;
        public MusicGroups musicGroup;
        public SampleClassification sampleClassificationNormal;
        public SampleClassification sampleClassificationConfident;
        public SampleClassification sampleClassificationDanger;
    }

    [System.Serializable]
    public class SampleClassification
    {
        public SampleType sampleType;
        public float classification;
    }

    [System.Serializable]
    public class MusicGroupClassification
    {
        public MusicGroups musicGroup;
        public SampleClassification sampleClassificationNormal;
        public SampleClassification sampleClassificationConfident;
        public SampleClassification sampleClassificationDanger;
    }

    public enum MusicGroups
    {
        percussion, woodwinds, strings, brass
    }

    public enum SampleType
    {
        normal, confident, danger
    }


    private void Start()
    {
        bps = 60.0f / bpm;
        eventPercussion = FMODUnity.RuntimeManager.CreateInstance(startingEventPercussion);
        eventPercussion.start();
        eventStrings = FMODUnity.RuntimeManager.CreateInstance(startingEventStrings);
        eventStrings.start();
        eventWoodwinds = FMODUnity.RuntimeManager.CreateInstance(startingEventWoodwinds);
        eventWoodwinds.start();
        eventBrass = FMODUnity.RuntimeManager.CreateInstance(startingEventBrass);
        eventBrass.start();
    }

    private void Update()
    {
        PlayAudioSourceOnBeat();

        if(oldMusicGroup != musicGroupChanged)
        {
            switch (musicGroupChanged)
            {
                case MusicGroups.percussion:
                    normal = valuesPercussion[0];
                    oldnormal = normal;
                    confident = valuesPercussion[1];
                    oldconfident = confident;
                    danger = valuesPercussion[2];
                    olddanger = danger;
                    break;
                case MusicGroups.woodwinds:
                    normal = valuesWoodwinds[0];
                    oldnormal = normal;
                    confident = valuesWoodwinds[1];
                    oldconfident = confident;
                    danger = valuesWoodwinds[2];
                    olddanger = danger;
                    break;
                case MusicGroups.strings:
                    normal = valuesStrings[0];
                    oldnormal = normal;
                    confident = valuesStrings[1];
                    oldconfident = confident;
                    danger = valuesStrings[2];
                    olddanger = danger;
                    break;
                case MusicGroups.brass:
                    normal = valuesBrass[0];
                    oldnormal = normal;
                    confident = valuesBrass[1];
                    oldconfident = confident;
                    danger = valuesBrass[2];
                    olddanger = danger;
                    break;
            }
            oldMusicGroup = musicGroupChanged;
        } 

        if (normal != oldnormal || confident != oldconfident || danger != olddanger)
        {
            oldnormal = normal;
            oldconfident = confident;
            olddanger = danger;
            MusicGroupClassification grp = new MusicGroupClassification
            {
                musicGroup = musicGroupChanged,
                sampleClassificationNormal = new SampleClassification(),
                sampleClassificationDanger = new SampleClassification(),
                sampleClassificationConfident = new SampleClassification()
            };
            grp.sampleClassificationConfident.classification = confident;
            grp.sampleClassificationDanger.classification = danger;
            grp.sampleClassificationNormal.classification = normal;
            List<MusicGroupClassification> mg = new List<MusicGroupClassification>();
            mg.Add(grp);
            ChangeMusic(mg);

            switch (musicGroupChanged)
            {
                case MusicGroups.percussion:
                    valuesPercussion[0] = normal;
                    valuesPercussion[1] = confident;
                    valuesPercussion[2] = danger;
                    break;
                case MusicGroups.woodwinds:
                    valuesWoodwinds[0] = normal;
                    valuesWoodwinds[1] = confident;
                    valuesWoodwinds[2] = danger;
                    break;
                case MusicGroups.strings:
                    valuesStrings[0] = normal;
                    valuesStrings[1] = confident;
                    valuesStrings[2] = danger;
                    break;
                case MusicGroups.brass:
                    valuesBrass[0] = normal;
                    valuesBrass[1] = confident;
                    valuesBrass[2] = danger;
                    break;
            }

        }
    }

    private MusicSample SearchForClosestMusicSample(MusicGroupClassification classificationToChangeToo)
    {
        List<MusicSample> musicSamplesOfGroup = new List<MusicSample>();
        //This can be updates if we separate all the music samples in lists of each group. For now only one list.
        foreach(MusicSample musicSample in musicSamples)
        {
            if(musicSample.musicGroup == classificationToChangeToo.musicGroup)
            {
                musicSamplesOfGroup.Add(musicSample);
            }
        }

        //To search for the closest music sample we calculate the distance, the same way as we would have done with vectors
        MusicSample closestMusicSample = null;
        float closestDistance = float.MaxValue;
        foreach(MusicSample musicSample in musicSamplesOfGroup)
        {
            float normalDist = Mathf.Pow(classificationToChangeToo.sampleClassificationNormal.classification - musicSample.sampleClassificationNormal.classification,2);
            float confidentDist = Mathf.Pow(classificationToChangeToo.sampleClassificationConfident.classification - musicSample.sampleClassificationConfident.classification, 2);
            float dangerDist = Mathf.Pow(classificationToChangeToo.sampleClassificationDanger.classification - musicSample.sampleClassificationDanger.classification, 2);

            float distance = normalDist + confidentDist + dangerDist;
            if(distance < closestDistance)
            {
                closestMusicSample = musicSample;
                closestDistance = distance;
            }
        }
        return closestMusicSample;
    }


    public void ChangeMusic(List<MusicGroupClassification> mgToChange)
    {

        foreach(MusicGroupClassification mgClassification in mgToChange)
        {
            MusicSample msSample = SearchForClosestMusicSample(mgClassification);
            switch (mgClassification.musicGroup)
            {
                case MusicGroups.percussion:
                    newEventPercussion = FMODUnity.RuntimeManager.CreateInstance(msSample.eventToBePlayed); 
                    if (!newEventPercussion.Equals(eventPercussion))
                    {
                        audioClipPercussionChanged = true;
                    }
                    break;
                case MusicGroups.strings:
                    newEventStrings = FMODUnity.RuntimeManager.CreateInstance(msSample.eventToBePlayed);
                    if (!newEventStrings.Equals(eventStrings))
                    {
                        audioClipStringsChanged = true;
                    }
                    break;
                case MusicGroups.woodwinds:
                    newEventWoodwinds = FMODUnity.RuntimeManager.CreateInstance(msSample.eventToBePlayed);
                    if (!newEventWoodwinds.Equals(eventWoodwinds))
                    {
                        audioClipWoodwindsChanged = true;
                    }
                    break;
                case MusicGroups.brass:
                    newEventBrass = FMODUnity.RuntimeManager.CreateInstance(msSample.eventToBePlayed);
                    if (!newEventBrass.Equals(eventBrass))
                    {
                        audioClipBrassChanged = true;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private int beat = 1;
    private double beatTotalLastFrame = -1;
    /// <summary>
    /// Plays audioSource on beat if audioclipchanged.
    /// </summary>
    private void PlayAudioSourceOnBeat()
    {
        double time = AudioSettings.dspTime;

        double beatTotal = time / bps;

        double beatTotalIntegerPart = Math.Truncate(beatTotal);

        //Starting time
        if(beatTotalLastFrame == -1)
        {
            beatTotalLastFrame = beatTotalIntegerPart;
        }

        //check if value changed, means that 1 beat has passed
        if(beatTotalLastFrame < beatTotalIntegerPart)
        {
            beatTotalLastFrame = beatTotalIntegerPart;
           // Debug.Log(beat);
            if(beat == 4)
            {
                beat = 1;
            } else
            {
                beat++;
            }
            int timelinePosition = 0;
            //eventPercussion.getTimelinePosition(out timelinePosition);
            //Debug.Log("Percussion Time: " + timelinePosition);
            //eventWoodwinds.getTimelinePosition(out timelinePosition);
            //Debug.Log("Woodwinds Time: " + timelinePosition);
            //eventStrings.getTimelinePosition(out timelinePosition);
            //Debug.Log("Strings Time: " + timelinePosition);
            //eventBrass.getTimelinePosition(out timelinePosition);
            //Debug.Log("Brass Time: " + timelinePosition);
            //timelinePosition = 0;
            if (audioClipPercussionChanged)
            {
                audioClipPercussionChanged = false;
                eventPercussion.getTimelinePosition(out timelinePosition);
                newEventPercussion.setTimelinePosition(timelinePosition + computerDelayMS);
                newEventPercussion.start();
                eventPercussion.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                eventPercussion.release();
                eventPercussion = newEventPercussion;
            }
            if (audioClipWoodwindsChanged)
            {
                audioClipWoodwindsChanged = false;
                eventWoodwinds.getTimelinePosition(out timelinePosition);
                newEventWoodwinds.setTimelinePosition(timelinePosition + computerDelayMS);
                newEventWoodwinds.start();
                eventWoodwinds.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                eventWoodwinds.release();
                eventWoodwinds = newEventWoodwinds;
            }
            if (audioClipStringsChanged)
            {
                audioClipStringsChanged = false;
                eventStrings.getTimelinePosition(out timelinePosition);
                newEventStrings.setTimelinePosition(timelinePosition + computerDelayMS);
                newEventStrings.start();
                eventStrings.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                eventStrings.release();
                eventStrings = newEventStrings;
            }
            if (audioClipBrassChanged)
            {
                audioClipBrassChanged = false;
                eventBrass.getTimelinePosition(out timelinePosition);
                newEventBrass.setTimelinePosition(timelinePosition + computerDelayMS);
                newEventBrass.start();
                eventBrass.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                eventBrass.release();
                eventBrass = newEventBrass;
            }

        }
    }

}
        
   

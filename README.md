More info about the project on **Dynamic Audio** can be found over at my site: [https://www.robincouwenberg.com/project/game-ai-project-dynamic-audio-erasmus-project/](https://www.robincouwenberg.com/project/game-ai-project-dynamic-audio-erasmus-project/)

The code is licensed under the MIT License.

The Audio assets ARE NOT INCLUDED! Do not use without written approval of myself beforehand.
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    FMOD.Studio.EventInstance bossArea;

    public FMODUnity.StudioEventEmitter bossAreaEmitter;


    // Use this for initialization
    void Start () {
        bossArea = bossAreaEmitter.EventInstance;
    }
	
	// Update is called once per frame
	void Update () {
	}


    public void UpdateHealth(float health)
    {
        bossArea.setParameterValue("Health", health);
    }

}
